Complete_dataset -> Raw dataset 

encoded_tags -> one hot encoded array of unique tags.

lda_training_content_tags -> for training LDA based on Analytics Vidya.

AV_predictions -> training topic feature vectors for ANN classifier.

AV_results_predicted -> 2D array with softmax probabilites of each predicted tags.


.ipynb -> Complete code for LDA and ANN training.

tag_model.h5 -> weight file for ANN classifier.

tag_model.json -> Architecture for ANN classifier.


Lemmatization not making any improvement.

num_topics, chunksize -> optimized based on perplexity and Coherent scores.